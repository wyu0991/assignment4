package main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

@SpringBootApplication
@ComponentScan({ "services" })
public class application extends Application {
	@FXML
	private BorderPane rootPane;

	public static void main(String[] args) {

		SpringApplication.run(application.class, args);
		launch(args);

	}

	public void start(Stage stage) throws Exception {
		FXMLLoader loader = new FXMLLoader(this.getClass().getResource("./Main.fxml"));
		loader.setController(ViewSwitcher.getInstance());
		Parent rootNode = loader.load();
		Scene scene = new Scene(rootNode);
		stage.setScene(scene);
		stage.setTitle("Assignment4");
		stage.show();

	}
}
