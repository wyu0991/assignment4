package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.sun.javafx.scene.paint.GradientUtils.Parser;

public class grabPerson {
	private static final String WS_URL = "http://localhost:8080";
    private static final String key="I am a good token";
	public static ArrayList<Person> fetchPeople(String token) throws IOException {
		ArrayList<Person> persons = new ArrayList<Person>();
		CloseableHttpResponse response = null;
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet(WS_URL + "/people");
		httpGet.setHeader("Authorization",token);
		httpGet.setHeader("Content-Type", "application/json");
		response = httpclient.execute(httpGet);
		if (response.getStatusLine().getStatusCode() != 200) {
			System.out.println("Its was not 200 it was " + response.getStatusLine());
			httpclient.close();
			return null;
		}
		HttpEntity entity = response.getEntity();
		String json = EntityUtils.toString(entity, StandardCharsets.UTF_8);
		JSONArray jsonArray = new JSONArray(json);
		for (Object obj : jsonArray) {
			JSONObject jsonObject = (JSONObject) obj;
			persons.add(new Person(jsonObject.getString("FirstName"),0,jsonObject.getString("LastName"),jsonObject.getString("DateOfBirth"),jsonObject.getInt("Id")));
		}
		EntityUtils.consume(entity);
		httpclient.close();
		response.close();
		ViewSwitcher.getInstance().setToken(token);
		updateperson(token);
       return persons;
	}

	public static int updateperson(String token) throws ClientProtocolException, IOException {
		// TODO Auto-generated method stub
		CloseableHttpResponse response = null;
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPut httpPut = new HttpPut(WS_URL + "/people/1");
		httpPut.setHeader("Authorization",token);
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("FirstName", "Bobby");
		jsonObject.put("Id", 1);
		jsonObject.put("LastName", "Federer");
		jsonObject.put("DateOfBirth", "08/08/81");
		jsonArray.put(jsonObject);
		StringEntity stringthing = new StringEntity(jsonArray.toString());
		//System.out.println(jsonArray.toString());
		httpPut.setEntity(stringthing);
		response = httpclient.execute(httpPut);
		//System.out.println(response.getStatusLine().getStatusCode());
		if (response.getStatusLine().getStatusCode() == 400) {
			System.out.println("Its was not 200 it was " + response.getStatusLine());
			httpclient.close();
			return -1;
		}
		if (response.getStatusLine().getStatusCode() != 200 && key.compareToIgnoreCase(token)!=0) {
			response.setStatusCode(401);
			System.out.println("Its was not 200 it was " + response.getStatusLine());
			httpclient.close();
			return -1;
		}
		if (response.getStatusLine().getStatusCode() != 200) {
			System.out.println("Its was not 200 it was " + response.getStatusLine());
			httpclient.close();
			return -1;
		}
		HttpEntity entity = response.getEntity();	
		String  id =jsonObject.get("Id").toString();
		int i =Integer.parseInt(id);
		return i;
		
	}

	public static int insertPerson(String token) throws ClientProtocolException, IOException {
		// TODO Auto-generated method stub
				CloseableHttpResponse response = null;
				CloseableHttpClient httpclient = HttpClients.createDefault();
				HttpPost httpPost = new HttpPost(WS_URL + "/people");
				httpPost.setHeader("Authorization",token);
				JSONArray jsonArray = new JSONArray();
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("FirstName", "Lou");
				jsonObject.put("Id", 4);
				jsonObject.put("LastName", "Smith");
				jsonObject.put("DateOfBirth", "01/01/90");
				jsonArray.put(jsonObject);
				StringEntity stringthing = new StringEntity(jsonArray.toString());
				httpPost.setEntity(stringthing);
				response = httpclient.execute(httpPost);
				if (response.getStatusLine().getStatusCode() == 400) {
					System.out.println("Its was not 200 it was " + response.getStatusLine());
					httpclient.close();
					return -1;
				}
				// TODO Auto-generated method stub
				HttpEntity entity = response.getEntity();
				String json = EntityUtils.toString(entity, StandardCharsets.UTF_8);
				JSONObject json1=new JSONObject(json);
				String  id =jsonObject.get("Id").toString();
				int i =Integer.parseInt(id);
				return i;
				
	}

	public static int DeletePerson(String token, int id) throws IOException {
		CloseableHttpResponse response = null;
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpDelete httpDelete = new HttpDelete(WS_URL + "/people/1");
		httpDelete.setHeader("Authorization",token);
		response=httpclient.execute(httpDelete);
		if (response.getStatusLine().getStatusCode() == 400) {
			System.out.println("Its was not 200 it was " + response.getStatusLine());
			httpclient.close();
			return -1;
		}
		if (response.getStatusLine().getStatusCode() != 200 && key.compareToIgnoreCase(token)!=0) {
			response.setStatusCode(401);
			System.out.println("Its was not 200 it was " + response.getStatusLine());
			httpclient.close();
			return -1;
		}
		if (response.getStatusLine().getStatusCode() != 200) {
			System.out.println("Its was not 200 it was " + response.getStatusLine());
			httpclient.close();
			return -1;
		}
		return 0;
		
		//return i;
	}

}
