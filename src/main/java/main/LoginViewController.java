package main;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

public class LoginViewController {

	@FXML
	private PasswordField password;

	@FXML
	private BorderPane rootPane;

	@FXML
	private TextField userName;

	@FXML
	void LoginButtonPressed(ActionEvent event) throws IOException, SQLException {
		String token = LoginSessionGateway.getInstance().Login(userName.getText(), password.getText());
		if (token != null) {
			ViewSwitcher.getInstance().switchView(ViewType.SecondView, null, -1, token);
		}
	}
}
