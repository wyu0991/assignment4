package main;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import db.DBController;
import db.DBGatewayMySQL;
import javafx.fxml.Initializable;

public class LoginSessionGateway implements Initializable{
	private static final String WS_URL = "http://localhost:8080";
	 private static LoginSessionGateway instance = null;
		public static LoginSessionGateway getInstance(){
	    	if (instance == null)
	    		instance = new LoginSessionGateway();
	    	return instance;
	    }
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}
	public String Login(String text, String text2) throws ClientProtocolException, IOException, SQLException {
         DBController dogcontroller = new DBController();
		 Connection connection = dogcontroller.startup();
		 String token = new DBGatewayMySQL(connection).authenticate(text,text2);
		 dogcontroller.setToken(token);
		//		CloseableHttpClient httpclient = HttpClients.createDefault();
//		JSONObject credentials = new JSONObject();
//		credentials.put("username", text);
//		credentials.put("password", text2);
//		
//		HttpPost loginReq = new HttpPost(WS_URL+"/login");
//		StringEntity reqEntity = new StringEntity(credentials.toString());
//		loginReq.setEntity(reqEntity);
//		loginReq.setHeader("Accept","application/json");
//		loginReq.setHeader("Content-Type","application/json");
//		CloseableHttpResponse response = httpclient.execute(loginReq);
//		
//		
//		if (response.getStatusLine().getStatusCode() == 401) {
//			System.out.println("Its was not 200 it was " + response.getStatusLine());
//			httpclient.close();
//			return null;
//		}
//		if (response.getStatusLine().getStatusCode() != 200) {
//			System.out.println("Its was not 200 it was " + response.getStatusLine());
//			httpclient.close();
//			return null;
//		}
//		String responseString=getResponseasString(response);
//		JSONObject responseJSON = new JSONObject(responseString);
//		String token = responseJSON.getString("session_id");
//		System.out.println(token);
//		ArrayList<Person> persons = grabPerson.fetchPeople(token);
//		if(persons!=null){ 
//    		for(int i=0;i<persons.size();i++){
//    			ViewSwitcher.getInstance().getPersons().add(persons.get(i));
//    		}
//		}
//		
		return token;
	}
	private String getResponseasString(CloseableHttpResponse response) throws ParseException, IOException {
		HttpEntity entity=response.getEntity();
		String strResponse= EntityUtils.toString(entity, StandardCharsets.UTF_8);
		EntityUtils.consume(entity);
		return strResponse;
		
	}

}
