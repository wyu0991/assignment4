package View;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import db.DBController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import main.Person;
import main.ViewSwitcher;
import main.ViewType;

public class personViewController implements Initializable {

	@FXML
	private TextField agefield;

	@FXML
	private TextField dateofbirth;

	@FXML
	private BorderPane rootPane;

	@FXML
	private TextField lastfield;

	@FXML
	private TextField firstfield;

	private Person person;
	private int check;

	private String token;

	@FXML
	void SaveButtonPressed(ActionEvent event) throws IOException, SQLException {
		Person oldperson = null;
		if(person!=null){
	    oldperson =new Person(person.getFirstName(),person.getAge(),person.getLastName(),person.getDateOfBirth(),person.getId());
		}
		if (person == null) {
			this.person = new Person(firstfield.getText(), Integer.parseInt(agefield.getText()), lastfield.getText(),
					dateofbirth.getText(), 0);
		} else {
			this.person.setFirstName(firstfield.getText());
			this.person.setLastName(lastfield.getText());
			this.person.setAge(Integer.parseInt(agefield.getText()));
			this.person.setDateOfBirth(dateofbirth.getText());
		}
		if (check == -1) {
			DBController.insertpeople(token, person);
			ViewSwitcher.getInstance().switchView(ViewType.SecondView, this.person, this.check, this.token);
		} else {
          
			DBController.getperson(token, person.getId(), this.person);
			DBController.ChangeAudit(oldperson, person);
			ViewSwitcher.getInstance().getPersons().set(this.check, this.person);
			ViewSwitcher.getInstance().switchView(ViewType.SecondView, this.person, this.check, this.token);
		}
	}

	@FXML
	void CancelButtonPressed(ActionEvent event) throws IOException {
		ViewSwitcher.getInstance().switchView(ViewType.SecondView, null, -1, this.token);
	}

	public personViewController(Person person, int check, String token) {
		this.token = token;
		this.check = check;
		if (person == null && check == -1) {
			return;
			// this.person=new Person("John", 0, "Doe", "0/00/00", check);
			// this.check=ViewSwitcher.getInstance().getPersons().size()+1;
			// this.token=token;
		} else {
			this.person = person;

		}

	}

	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		if (person != null) {
			firstfield.setText(this.person.getFirstName());
			lastfield.setText(this.person.getLastName());
			agefield.setText(String.valueOf(this.person.getAge()));
			dateofbirth.setText(this.person.getDateOfBirth());
		}
	}

}
