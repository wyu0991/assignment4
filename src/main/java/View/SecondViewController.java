package View;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ResourceBundle;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;

import db.Audit;
import db.DBController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import main.Person;
import main.ViewSwitcher;
import main.ViewType;
import main.grabPerson;

public class SecondViewController implements Initializable {

	@FXML
    private Tab Audit_trail;

    @FXML
    private ListView<String> Auditlist;

    @FXML
    private Tab ListTab;

	@FXML
	private ObservableList<main.Person> persons;
	@FXML
	private ListView<String> PersonDisplayList;

	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public SecondViewController(Person person, int check, String token) {
		persons = ViewSwitcher.getInstance().getPersons();
		this.setToken(token);
	}

	@FXML
	void DeleteButtonPressed(ActionEvent event) throws IOException {
		int check = PersonDisplayList.getSelectionModel().getSelectedIndex();

		if (check == -1) {
			System.out.println("ERROOOORR");
			return;
		}
//		System.out.println(ViewSwitcher.getInstance().getPersons().get(check).getFirstName());
//		System.out.println(check);
		if (check > -1) {
			DBController.deleteperson(token, ViewSwitcher.getInstance().getPersons().get(check).getId());
			ViewSwitcher.getInstance().getPersons().remove(check);
			ViewSwitcher.getInstance().switchView(ViewType.SecondView, null, -1, token);
		}
	}

	@FXML
	void addPersonButtonPressed(ActionEvent event) throws IOException {
		int check = PersonDisplayList.getSelectionModel().getSelectedIndex();
		if (check > -1) {
			ViewSwitcher.getInstance().switchView(ViewType.PersonView, persons.get(check), check, token);
		}
		if (check == -1) {
			ViewSwitcher.getInstance().switchView(ViewType.PersonView, null, -1, token);

		}
	}

	@FXML
	void getPeople(ActionEvent event) throws JSONException, SQLException, IOException {
		System.out.println(token);
		if (token == DBController.getToken()) {
			ResponseEntity<String> listpeople = DBController.fetchpeoples(token);
			JSONArray peoples = new JSONArray(listpeople.getBody());
			if (peoples.isEmpty()) {
				return;
			}
			JSONObject thing = peoples.getJSONObject(0);
			// Person person = new Person(token, 0, token, token, 0);
			ViewSwitcher.getInstance().getPersons().removeAll(persons);
			for (Object obj : peoples) {
				JSONObject jsonObject = (JSONObject) obj;
				Person person = new Person(jsonObject.optString("first_name"), jsonObject.optInt("age"),
						jsonObject.optString("last_name"), jsonObject.optString("date_birth"), jsonObject.optInt("id"));
				ViewSwitcher.getInstance().getPersons().add(person);
			}

		}
		ViewSwitcher.getInstance().switchView(ViewType.SecondView, null, -1, token);
	}
	  @FXML
	    void Audit_Button(ActionEvent event) throws SQLException, IOException {
		  int check = PersonDisplayList.getSelectionModel().getSelectedIndex();
		  int i =0;
		 Auditlist.getItems().clear();
		  if(check > -1){
			  ObservableList<Audit> audtis = DBController.getAudit(ViewSwitcher.getInstance().getPersons().get(check).getId(),token);	 
			 while(i<audtis.size()){
				 String name = persons.get(check).getFirstName();
				 Auditlist.getItems().add("Date:"+audtis.get(i).getDate()+" By:"+name+" action:"+audtis.get(i).getMessage());
				 i+=1;
			 }
		  }
		 // ViewSwitcher.getInstance().switchView(ViewType.SecondView, null, -1, token);
		 
	    }

	public void initialize(URL arg0, ResourceBundle arg1) {
		for (int i = 0; i < persons.size(); i++) {
			String person2 = persons.get(i).getFirstName() + " " + persons.get(i).getLastName() + " "
					+ persons.get(i).getAge() + " " + persons.get(i).getDateOfBirth();
			PersonDisplayList.getItems().add(person2);
		}
	}

}
