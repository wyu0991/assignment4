package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.ietf.jgss.GSSException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import main.Person;

public class DBGatewayMySQL {
	private static Connection connection;

	public DBGatewayMySQL(Connection connection) {
		this.connection = connection;
	}

	public String authenticate(String text, String text2) throws SQLException {
		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * FROM logins");
		String username = null;
		String password = null;
		while (rs.next()) {
			username = rs.getString("user_name");
			password = rs.getString("password");
			if (username.equals(text) && password.equals(text2)) {
				PreparedStatement stmt1 = connection
						.prepareStatement("INSERT INTO `session` (`session_id`) VALUES (?)");
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
				LocalDateTime now = LocalDateTime.now();
				stmt1.setObject(1, dtf.format(now));
				stmt1.executeUpdate();
				return dtf.format(now);
			}
		}
		return null;
	}

	public static JSONArray getPeople(String token) throws SQLException {
		if (token != DBController.getToken()) {
			return null;
		}
		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * FROM people");// TODO
																	// Auto-generated
																	// method
																	// stub
		JSONArray people = new JSONArray();
		while (rs.next()) {
			JSONObject person = new JSONObject();
			person.put("first_name", rs.getString("first_name"));
			person.put("last_name", rs.getString("last_name"));
			person.put("age", rs.getInt("age"));
			person.put("id", rs.getInt("id"));
			person.put("date_birth", rs.getString("date_birth"));
			people.put(person);
		}
		return people;
	}

	public ResponseEntity<String> updateperson(int id, Person person) throws SQLException {
		PreparedStatement stmt;
		stmt = connection.prepareStatement(
				"UPDATE `people` SET `first_name` = ?, `last_name` = ?,`age` = ?, `date_birth` = ? WHERE `people`.`id` = ?");
		if (person.getFirstName().length() > 100 || person.getFirstName().length() < 1) {
			return new ResponseEntity<String>("Error", HttpStatus.valueOf(400));
		}
		if (person.getLastName().length() > 100 || person.getLastName().length() < 1) {
			return new ResponseEntity<String>("Error", HttpStatus.valueOf(400));
		}
		stmt.setObject(1, person.getFirstName());
		stmt.setObject(2, person.getLastName());
		stmt.setObject(3, person.getAge());
		stmt.setObject(4, person.getDateOfBirth());
		stmt.setObject(5, id);
		try {
			stmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			return new ResponseEntity<String>("", HttpStatus.valueOf(404));
		}
		// TODO Auto-generated method stub
		return new ResponseEntity<String>("insert successful", HttpStatus.valueOf(200));

	}

}
