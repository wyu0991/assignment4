package db;

import java.sql.Date;

public class Audit {
	private String message;
	private Date date;
	private int id;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Audit(Date date, int int1, String string) {
		this.date=date;
		this.id=int1;
		this.message=string;// TODO Auto-generated constructor stub
	}

}
