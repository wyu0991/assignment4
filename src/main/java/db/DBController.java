package db;

import org.springframework.web.bind.annotation.RestController;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import main.Person;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;

@RestController
public class DBController {
	private static Connection connection;
	private static String token;
	 private static ObservableList<Audit> audits=FXCollections.observableArrayList();
	public static String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public static Connection startup() {
		connection = DBConnection.connectToDB();
		return connection;
	}

	public static void cleanup() throws SQLException {
		connection.close();
	}

	@GetMapping("/people")
	public static ResponseEntity<String> fetchpeoples(@RequestHeader("Authorization") String token)
			throws JSONException, SQLException {
		if (!token.equals(DBController.getToken())) {
			return new ResponseEntity<String>("", HttpStatus.valueOf(401));
		}

		JSONArray people = DBGatewayMySQL.getPeople(token);
		if (people == null) {
			return new ResponseEntity<String>("Error ", HttpStatus.valueOf(401));
		}
		ResponseEntity<String> response = new ResponseEntity<String>(people.toString(), HttpStatus.valueOf(201));
		return response;
	}

	@GetMapping("/people/{id}")
	public static ResponseEntity<String> getperson(@RequestHeader("Authorization") String token,
			@PathVariable("id") int id, Person person) throws JSONException, SQLException {
		if (!token.equals(DBController.getToken())) {
			return new ResponseEntity<String>("", HttpStatus.valueOf(401));
		}

		ResponseEntity<String> thing = new DBGatewayMySQL(connection).updateperson(id, person);
		if (thing.getStatusCodeValue() == 404) {
			return new ResponseEntity<String>("", HttpStatus.valueOf(404));
		}
		JSONObject person1 = new JSONObject();
		person1.put("id", person.getId());
		person1.put("name", person.getFirstName());
		person1.put("age", person.getAge());

		ResponseEntity<String> response = new ResponseEntity<String>(person1.toString(), HttpStatus.valueOf(200));

		return response;

	}

	@PostMapping("/people")
	public static ResponseEntity<String> insertpeople(String token, Person person) throws SQLException {
		if (!token.equals(DBController.getToken())) { // left off here
			return new ResponseEntity<String>("", HttpStatus.valueOf(401));
		}
		PreparedStatement stmt1 = connection.prepareStatement(
				"INSERT INTO `people`(`first_name`, `last_name`, `age`, `date_birth`) VALUES (?,?,?,?)");
		if (person.getFirstName().length() > 100 || person.getFirstName().length() < 1) {
			return new ResponseEntity<String>("Error", HttpStatus.valueOf(400));
		}
		if (person.getLastName().length() > 100 || person.getLastName().length() < 1) {
			return new ResponseEntity<String>("Error", HttpStatus.valueOf(400));
		}
		stmt1.setObject(1, person.getFirstName());
		stmt1.setObject(2, person.getLastName());
		stmt1.setObject(3, person.getAge());
		stmt1.setObject(4, person.getDateOfBirth());
		stmt1.executeUpdate();
		PreparedStatement stmt11 = connection.prepareStatement("SELECT * FROM `people` WHERE `first_name`=?");
		stmt11.setObject(1, person.getFirstName());
		ResultSet rs = stmt11.executeQuery();
		rs.next();
		person.setId(rs.getInt("id"));
		PreparedStatement stmt2 = connection.prepareStatement(
				"INSERT INTO `audit_trail`(`change_msg`,`person_id`) VALUES (?,?)");
		stmt2.setObject(1, "added");
		stmt2.setObject(2,person.getId());
		stmt2.executeUpdate();
		JSONObject person1 = new JSONObject();
		person1.put("id", person.getId());
		person1.put("name", person.getFirstName());
		person1.put("age", person.getAge());
		return new ResponseEntity<String>("hello inserted", HttpStatus.valueOf(200));
	}

	public static ResponseEntity<String> deleteperson(String token2, int id) {
		// TODO Auto-generated method stub
		if (!token2.equals(DBController.getToken())) { // left off here
			return new ResponseEntity<String>("", HttpStatus.valueOf(401));
		}
		try {
			PreparedStatement stmt1 = connection.prepareStatement("DELETE FROM `people` WHERE `people`.`id` = ?");
			stmt1.setObject(1, id);
			stmt1.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			return new ResponseEntity<String>("", HttpStatus.valueOf(404));
		}
		return new ResponseEntity<String>("delete successful", HttpStatus.valueOf(200));

	}

	public static ObservableList<Audit> getAudit(int i, String token2) throws SQLException {
		// TODO Auto-generated method stub
		PreparedStatement stmt11;
		try {
			audits.clear();
			stmt11 = connection.prepareStatement("SELECT * FROM `audit_trail` WHERE `person_id`=?");
			stmt11.setObject(1, i);
			ResultSet rs = stmt11.executeQuery();
			while(rs.next()){
				 Audit audit = new Audit(rs.getDate(4),rs.getInt(3),rs.getString(5));
				audits.add(audit);
			}
			return audits;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}

	public static void ChangeAudit(Person oldperson, Person person) throws SQLException {
		if (oldperson==null){
			System.out.println("OLDPERSSON EMPTY");
			return;
		}
		if(oldperson.getAge()!=person.getAge()){
			PreparedStatement stmt2 = connection.prepareStatement(
					"INSERT INTO `audit_trail`(`change_msg`,`person_id`) VALUES (?,?)");
			stmt2.setObject(1, "changed from "+oldperson.getAge()+" to "+person.getAge());
			stmt2.setObject(2,person.getId());
			stmt2.executeUpdate();
		}
		if(oldperson.getFirstName().equals(person.getFirstName())==false){
			PreparedStatement stmt2 = connection.prepareStatement(
					"INSERT INTO `audit_trail`(`change_msg`,`person_id`) VALUES (?,?)");
			stmt2.setObject(1, "changed from "+oldperson.getFirstName()+" to "+person.getFirstName());
			stmt2.setObject(2,person.getId());
			System.out.println("IN FUNCTION "+stmt2.executeUpdate());
		}
		if(oldperson.getLastName().equals(person.getLastName())==false){
			PreparedStatement stmt2 = connection.prepareStatement(
					"INSERT INTO `audit_trail`(`change_msg`,`person_id`) VALUES (?,?)");
			stmt2.setObject(1, "changed from "+oldperson.getLastName()+" to "+person.getLastName());
			stmt2.setObject(2,person.getId());
			stmt2.executeUpdate();
		}
		if(oldperson.getAge()!=person.getAge()){
			PreparedStatement stmt2 = connection.prepareStatement(
					"INSERT INTO `audit_trail`(`change_msg`,`person_id`) VALUES (?,?)");
			stmt2.setObject(1, "changed from "+oldperson.getAge()+" to "+person.getAge());
			stmt2.setObject(2,person.getId());
			stmt2.executeUpdate();
	}
	
	}
		
			// TODO Auto-generated method stub
		
	}

