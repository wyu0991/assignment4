package db;

import com.mysql.cj.jdbc.MysqlDataSource;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection {

    public static Connection connectToDB()  {
        //connect to data source and create a connection instance
        Properties props=null;
		props = getConfig("/db.properties");

        //create the datasource
        MysqlDataSource ds = new MysqlDataSource();
        ds.setURL(props.getProperty("MYSQL_DB_URL"));
        ds.setUser(props.getProperty("MYSQL_DB_USERNAME"));
        ds.setPassword(props.getProperty("MYSQL_DB_PASSWORD"));

        //create the connection
        try {
			return ds.getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("HELLLLLLOOOOO");
			e.printStackTrace();
		}
		return null;
    }

    private static Properties getConfig(String propsFileName)  {
        Properties props = new Properties();

        BufferedInputStream propsFile = (BufferedInputStream) DBConnection.class.getResourceAsStream(propsFileName);
        try {
			props.load(propsFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
			propsFile.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        return props;
    }
}
